//
//  AlertView.swift
//  EvilCorpNews
//
//  Created by Tailor, Chirag on 29/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //Generic AlertView functions requires title and messages to be passed
    func showAlertWithOk(title alertTitle:String, message alertMessage:String)
    {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        let okAction = UIAlertAction(title: ALERT_DISMISS_BUTTON, style: .default)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}
