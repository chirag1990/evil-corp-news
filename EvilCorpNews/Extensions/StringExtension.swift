//
//  StringExtension.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 29/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import Foundation

extension String {
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
