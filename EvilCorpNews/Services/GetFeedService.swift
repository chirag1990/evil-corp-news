//
//  GetFeedService.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 23/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import RealmSwift


class GetFeedService {
    //Creating a singleton for GetFeedService()
    static let instance = GetFeedService()
    
    // Initialising Realm
    let realm : Realm = try! Realm()
    
    // creating datasource with get which will return article list
    // this will be used when user has internet access
    var dataSource : [Articles] {
        get {
            return articleList
        }
    }
    // same as above, this should be used if the application will run directly from realm instead of api
    var dataSourceForRealm : [Articles] {
        get {
            return articleListForRealm
        }
    }
    
    // getObject will fetch all the objects in realm to make the offline state to work
    func getObjects(type: Object.Type) -> Results<Object>? {
        return realm.objects(type)
    }
    
    // creating a fileprivate of list
    fileprivate var articleList = [Articles]()
    fileprivate var articleListForRealm = [Articles]()
    
    
    func getNewsFeed(completion: @escaping CompletionHandler) {
        //creating alamofire request
        Alamofire.request(API_URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            //checking if response error returned is nil
            if response.result.error == nil {
                
                // store the response result value in response
                guard let response = response.result.value as? [String:Any] else {
                    completion(false)
                    return
                }
                
                // store the response list of articles in data
//                guard let data = response["articles"] as? [[String:Any]] else {
//                    completion(false)
//                    return
//                }
                // making sure articleList is empty before adding new data to list
                self.articleList.removeAll()
                
                //iterate through the data and store it in articeList
//                for record in data {
//                    let articleData = Articles(JSON: record )
//                    self.articleList.append(articleData!)
//                }
                
                // call the function for saving to realm and pass the full response
                self.saveToRealm(response: response)
                
                //print(self.articleList)
                completion(true)
                
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
            
        }
        
    }
    
    func saveToRealm(response : [String:Any])
    {
        // store response articles in res
        if let res = response["articles"] as? NSArray
        {
            //iterate through data in res
            for data in res
            {
                // store data dictionary into article
                if let article = data as? NSDictionary
                {
                    // if the realm is used for complete application we require to append data to articleListForRealm
                    let art = Articles(JSON: article as! [String : Any])
                    self.articleList.append(art!)
                    do {
                        //writing to realm file
                        try self.realm.write{
                            self.realm.add(self.articleList, update: true)
                        }
                    }catch{
                        //TODO print error
                    }
                }
            }
        }
    }
    
    // function to retrive image through url
    func getImage(_ url:String,handler: @escaping (UIImage?)->Void) {
        Alamofire.request(url, method: .get).responseImage { response in
            if let data = response.result.value {
                handler(data)
            } else {
                handler(nil)
            }
        }
    }
}
