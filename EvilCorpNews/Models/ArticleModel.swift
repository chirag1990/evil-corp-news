//
//  ArticleModel.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 23/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
import ObjectMapper_Realm

class ArticleModel: Object, Mappable {
    
    var articles : List<Articles>! = nil
    @objc dynamic var error: Bool = false
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        error        <- map["error"]
        articles     <- (map["articles"])
    }
}

class Articles : Object, Mappable {
    
    @objc dynamic var articleModel: ArticleModel! = nil
    @objc dynamic var body: String!
    @objc dynamic var commentCount: Int = 0
    @objc dynamic var createdAt: String!
    @objc dynamic var datePublished: String!
    
    @objc dynamic var feedId: Int = 0
    @objc dynamic var hasLiked: Bool = false
    @objc dynamic var id: Int = 0
    @objc dynamic var image: String!
    @objc dynamic var likeCount: Int = 0
    @objc dynamic var published: Int = 0
    @objc dynamic var shares: Int = 0
    @objc dynamic var summary: String!
    @objc dynamic var title: String!
    @objc dynamic var updatedAt: String!
    @objc dynamic var url: String!
    var  feed : List<Feed>! = nil
    var industries: List<Industries>! = nil
    //var sectors: List<Sectors>! = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        articleModel        <- map["articleModel"]
        body                <- (map["body"])
        commentCount        <- map["comment_count"]
        createdAt           <- (map["created_at"])
        datePublished       <- map["date_published"]
        feedId              <- (map["feed_id"])
        hasLiked            <- map["has_liked"]
        id                  <- (map["id"])
        image               <- map["image"]
        likeCount           <- (map["like_count"])
        published           <- map["published"]
        shares              <- (map["shares"])
        summary             <- map["summary"]
        updatedAt           <- (map["updated_at"])
        url                 <- map["url"]
        title               <- map["title"]
        id                  <- (map["id"])
        feed                <- map["feed"]
        industries          <- map["industries"]
        //sectors        <- map["sectors"]
    }
}

class Feed: Object, Mappable {
    
    @objc dynamic var article: Articles! = nil
    @objc dynamic var active: Int = 0
    @objc dynamic var autoPublish: Int = 0
    @objc dynamic var coverImage: String!
    @objc dynamic var createdAt: String!
    @objc dynamic var descriptionField: String!
    @objc dynamic var id: Int = 0
    @objc dynamic var lastSynced: String!
    @objc dynamic var logo: String!
    @objc dynamic var name: String!
    @objc dynamic var targetCountryId: Int = 0
    @objc dynamic var updatedAt: String!
    @objc dynamic var url: String!
    
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        article                 <- map["article"]
        active                  <- (map["active"])
        autoPublish             <- map["auto_publish"]
        createdAt               <- (map["created_at"])
        coverImage              <- map["cover_image"]
        descriptionField        <- (map["descriptions"])
        lastSynced              <- map["last_synced"]
        id                      <- (map["id"])
        logo                    <- map["logo"]
        name                    <- (map["name"])
        targetCountryId         <- map["target_country_id"]
        updatedAt               <- (map["updated_at"])
        url                     <- map["url"]
    }
}

class Industries: Object, Mappable {
    
    @objc dynamic var article: Articles! = nil
    @objc dynamic var id: Int = 0
    @objc dynamic var name: String!
    @objc dynamic var slug: String!
    
    required convenience init?(map: Map) {
        self.init()
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        article                 <- map["article"]
        id                      <- (map["id"])
        name                    <- (map["name"])
        slug                    <- map["slug"]
    }
    
}

//class Sectors : Object {
//
//
//}
