//
//  NewsFeedTableViewCell.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 23/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class NewsFeedTableViewCell: UITableViewCell {

    //Outlets
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    
    // used to set the data in cell
    var newsRecord:Articles!{
        didSet{
            self.newsTitle.numberOfLines = 0
            self.newsTitle.text = newsRecord.title
            self.newsTitle.sizeToFit()
            self.newsDate.text = newsRecord.datePublished
            GetFeedService.instance.getImage(newsRecord.image) { (image) in
                if image != nil {
                    self.newsImage.image = image
                }
            }
        }
    }
    
}

extension NewsFeedTableViewCell {
    // setting up cell
    struct CellConstant {
        static let Identifier = CELL_IDENTIFIER
        static let Height:CGFloat = 120.0
    }
    
    // registing the cell class
    static func registerClass(tableView:UITableView){
        tableView.register(NewsFeedTableViewCell.self, forCellReuseIdentifier: CellConstant.Identifier)
    }
    
    // registering the nib
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: CELL_NIB_NAME, bundle: nil), forCellReuseIdentifier: CellConstant.Identifier)
    }
}
