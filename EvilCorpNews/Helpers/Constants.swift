//
//  Constants.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 23/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import Foundation

typealias CompletionHandler = (_ Success: Bool) -> ()

//API URL
let API_URL = "https://admin.fluxible.co/api/articles?token=123&api_token=XSiZx73hQrZRi8d9"

//HEADER
let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]

//SEGUES
let DETAILS_SEGUE_IDENTIFIER = "ShowNewsDetailsSegue"

//FONTS
let FONT_AVENIR_NEXT_REGULAR = "Avenir Next Regular"

//IDENTIFIERS and NIB NAME
let CELL_IDENTIFIER = "NewsFeedCell"
let CELL_NIB_NAME = "NewsFeedTableViewCell"
let NEWSFEEDDETAILSVC_IDENTIFIER = "NewsFeedDetailsViewController"

//ALERT
let ALERT_TITLE = "Evil Corp News"
let ALERT_MESSAGE = "There was an issue with your connection. Therefore we haven't been able to fetch any data. Please try again later"
let ALERT_DISMISS_BUTTON = "Dimiss"


//DZNEmptyDataSet String
let DESCRIPTION_TEXT = "There was no internet connection, we could not retrieve any data. Please click try again button when internet is available."
let TRY_AGAIN_TEXT = "Try Again"
let TITLE_TEXT = "Oh No!"
let IMAGE_NAME_TEXT = "no-wifi"
