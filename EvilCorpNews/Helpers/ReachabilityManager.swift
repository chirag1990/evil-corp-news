//
//  Reachability.swift
//  EvilCorpNews
//
//  Created by Tailor, Chirag on 29/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import Foundation
import Alamofire

class ReachabilityManager {
    
    class func isConnected() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
