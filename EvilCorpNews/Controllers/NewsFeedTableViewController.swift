//
//  NewsFeedTableViewController.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 23/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import UIKit

class NewsFeedTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet var tableview: UITableView!
    
    //Variables to pass news data to details controller
    var newsTitle : String?
    var newsImageUrl : String?
    var newsDate : String?
    var newsFeed : String?
    
    //Datasource
    fileprivate var dataSource : [Articles] = []
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSettings()
    }

    fileprivate func initialSettings() {
        // setting up the delegate and datasource for tableview
        tableview.delegate = self
        tableview.dataSource = self
        
        // setting up the delegate and datasource for DZNEmptyDataSet
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        registerForPreviewing(with: self, sourceView: view)
        
        // registering the cell
        NewsFeedTableViewCell.registerNib(tableView: tableview)
        self.loadData()
    }
    
    fileprivate func loadData() {
        //making sure dataSource is Empty
        dataSource = []
        
        GetFeedService.instance.getNewsFeed { (success) in
            if success {
                // we know api has parsed and therefore realm object is created. We can now use it
                self.loadFromRealmObject()
                self.tableview.reloadData()
            } else {
                // we need to check if the realm object is available if so we can use it.
                self.loadFromRealmObject()
                self.tableview.reloadData()
            }
        }
        
        return
    }
    
    fileprivate func loadFromRealmObject()
    {
        let realmData = GetFeedService.instance.getObjects(type: Articles.self)
        
        if (realmData?.isEmpty)! {
            DispatchQueue.main.async {
                self.showAlertWithOk(title: ALERT_TITLE, message: ALERT_MESSAGE)
            }
        } else {
            if let realmObjects = realmData {
                for element in realmObjects {
                    if let articles = element as? Articles {
                        self.dataSource.append(articles)
                    }
                }
            }
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // return number of sections, currently there is only 1 section
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // count datasource to get number of cells
        return self.dataSource.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // dequeue reusable cell
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsFeedTableViewCell.CellConstant.Identifier) as! NewsFeedTableViewCell
        // call the newsRecord didSet method for dataSource indexPath.row
        cell.newsRecord = dataSource[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //estimate height of cell
        return NewsFeedTableViewCell.CellConstant.Height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // setting up the local variables with selected data
        self.newsTitle = self.dataSource[indexPath.row].title
        self.newsImageUrl = self.dataSource[indexPath.row].image
        self.newsDate = self.dataSource[indexPath.row].datePublished
        self.newsFeed = self.dataSource[indexPath.row].body
        // setting up perform segue
        self.performSegue(withIdentifier: DETAILS_SEGUE_IDENTIFIER, sender: nil)
    }
    
    // MARK: - DZNEmptyDataSet Methods
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = TITLE_TEXT
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = DESCRIPTION_TEXT
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .body)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: IMAGE_NAME_TEXT)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView, for state: UIControl.State) -> NSAttributedString? {
        let str = TRY_AGAIN_TEXT
        let attrs = [NSAttributedString.Key.font: UIFont.preferredFont(forTextStyle: .callout)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func emptyDataSet(_ scrollView: UIScrollView, didTap button: UIButton) {
        self.loadData()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == DETAILS_SEGUE_IDENTIFIER) {
            // passing the data to details view controller
            let vc = segue.destination as! NewFeedDetailsViewController
            vc.newsFeedTitle = self.newsTitle
            vc.newsImageUrl = self.newsImageUrl
            vc.newsFeedDate = self.newsDate
            vc.newsFeedBody = self.newsFeed
        }
    }
}

//Implementing 3d Touch to tableview cell
extension NewsFeedTableViewController : UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        // getting the indexpath for the row selected
        guard let indexPath = tableview.indexPathForRow(at: location),
            let cell = tableview.cellForRow(at: indexPath) else {
                debugPrint("Coult not get hold of indexpath")
                return nil
        }
        // initialising the viewcontroller
        guard let newsFeedDetailsViewController = storyboard?.instantiateViewController(withIdentifier: NEWSFEEDDETAILSVC_IDENTIFIER) as? NewFeedDetailsViewController else {
            debugPrint("Could not instantiate the view controller ")
            return nil
        }
        
        // passing the data for cell selected
        newsFeedDetailsViewController.newsFeedTitle = self.dataSource[indexPath.row].title
        newsFeedDetailsViewController.newsImageUrl = self.dataSource[indexPath.row].image
        newsFeedDetailsViewController.newsFeedDate = self.dataSource[indexPath.row].datePublished
        newsFeedDetailsViewController.newsFeedBody = self.dataSource[indexPath.row].body
        
        previewingContext.sourceRect = cell.frame
        
        return newsFeedDetailsViewController
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        show(viewControllerToCommit, sender: self)
    }
    
    
}
