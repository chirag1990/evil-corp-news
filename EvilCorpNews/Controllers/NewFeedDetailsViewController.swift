//
//  NewFeedDetailsViewController.swift
//  EvilCorpNews
//
//  Created by Chirag Tailor on 24/10/2018.
//  Copyright © 2018 TCT Apps. All rights reserved.
//

import UIKit

class NewFeedDetailsViewController: UIViewController {
    // outlets
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsBody: UITextView!
    
    // variable required for passed data
    var newsFeedTitle : String?
    var newsImageUrl : String?
    var newsFeedDate : String?
    var newsFeedBody : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpDetailsView()
    }
    
    func setUpDetailsView()
    {
        
        // setting up details view with data passed.
        newsTitle.numberOfLines = 0
        newsTitle.text = self.newsFeedTitle
        newsTitle.sizeToFit()
        newsDate.text = self.newsFeedDate
        
        // parsing HTML formatted text in to plain text
        let attrStr = NSMutableAttributedString(attributedString: (self.newsFeedBody?.html2AttributedString)!)
        attrStr.addAttribute(.font, value: UIFont(name: FONT_AVENIR_NEXT_REGULAR, size: 20) ?? .systemFont(ofSize: 20), range: .init(location: 0, length: (self.newsFeedBody?.html2String.count)!))
        
        newsBody.attributedText = attrStr
        
        // parsing the image
        if let newsFeedImage = self.newsImageUrl {
            GetFeedService.instance.getImage(newsFeedImage) { (image) in
                if image != nil {
                    self.newsImage.image = image
                }
            }
        }
        else {
            debugPrint("There was error getting the image")
            return
        }
    }
}
